class ServerConfig {

  static const domainNameServer = 'http://booksys.hcs.sy';

  static const register = '/api/auth/register';
  static const login = '/api/auth/login';
  static const check_valid = '/api/auth/check_validity';
  static const getCategories = '/api/category/get_all';


}