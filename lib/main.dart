import 'package:flutter/material.dart';
import 'package:flutter_course/getx_bindings/categories_binding.dart';
import 'package:flutter_course/modules/Home_page/homePage.dart';
import 'getx_bindings/home_page_binding.dart';
import 'package:flutter_course/modules/landing/landing.dart';
import 'getx_bindings/landing_binding.dart';
import 'package:flutter_course/modules/login/login.dart';
import 'package:flutter_course/modules/register/register.dart';
import 'package:flutter_course/modules/splash/splash.dart';
import 'getx_bindings/splash_binding.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'getx_bindings/login_binding.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'getx_bindings/register_binding.dart';
import 'modules/categories/Categories.dart';


void main() {
  runApp(
     GetMaterialApp(
      initialRoute:'/login',
      getPages: [
        GetPage(name: '/login', page:()=> Login(), binding: LoginBinding()),
        GetPage(name: '/landing', page:()=> Landing(), binding: LandingBinding()),
        GetPage(name: '/register', page:()=> Register(), binding: RegisretBinding()),
        GetPage(name: '/home', page:()=> HomePage(), binding: HomePageBinding()),
        GetPage(name: '/splash', page:()=> Splash(), binding: SplashBinding()),
        GetPage(name: '/categories', page: ()=>Categories(),binding: CategoriesBinding()),

      ],
       builder: EasyLoading.init(),

    )
  );
}
