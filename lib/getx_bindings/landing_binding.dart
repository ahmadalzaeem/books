import 'package:flutter_course/modules/landing/landing_controller.dart';
import 'package:get/get.dart';

class LandingBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<LandingController>(LandingController());
  }
}
