import 'package:flutter_course/modules/register/register_controller.dart';
import 'package:get/get.dart';

class RegisretBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<RegisterController>(RegisterController());
  }
}
