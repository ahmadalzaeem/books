import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final double height, width;
  final String buttonName;
  final Color? ButtonColor;
  final double ? fontSize;
  final Function() onTap;

  CustomButton({
    required this.height,
    required this.width,
    required this.buttonName,
    required this.onTap,
    this.ButtonColor,
    this.fontSize,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap : onTap,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: ButtonColor ?? Colors.white,
        ),
        child: Center(
          child: Text(
            buttonName,
            style: TextStyle(
              fontSize: fontSize,
              color: Colors.black,
            ),
          ),
        ),
      ),
    );
  }
}
