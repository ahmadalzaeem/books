import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final double width, height;
  final String hintText;
  final TextInputType ? keyboard;
  final Function (String) onChange;

  CustomTextField({
    required this.width,
    required this.height,
    required this.hintText,
    this.keyboard,
    required this.onChange,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Colors.white.withAlpha(20),
        border: Border(
          top: BorderSide(width: 2, color: Colors.white.withAlpha(30)),
          right: BorderSide(width: 2, color: Colors.white.withAlpha(30)),
          bottom: BorderSide(width: 2, color: Colors.white.withAlpha(30)),
          left: BorderSide(width: 2, color: Colors.white.withAlpha(30)),
        ),
      ),
      child: Center(
        child: TextField(
          onChanged:onChange,
          cursorColor: Colors.white,
          keyboardType: keyboard ?? TextInputType.text,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(
              fontSize: 16,
              color: Colors.grey,
            ),
            contentPadding: EdgeInsets.fromLTRB(4, 4, 4, 4),
          ),
        ),
      ),
    );
  }
}
