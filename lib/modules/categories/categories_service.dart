import 'package:flutter_course/config/server_config.dart';
import 'package:flutter_course/models/categories_model.dart';
import 'package:http/http.dart' as http;
class CategoriesService{

  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.getCategories);

  Future<List<Category>> getCategories(String token) async{
    var response = await http.get(url , headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    });
    print(response.statusCode);
    print(response.body);

    if(response.statusCode == 200){
      var categories = categoriesFromJson(response.body);
      return categories.categories;
    }
    else {
      return [];
    }
  }
}