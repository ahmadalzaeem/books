import 'package:flutter/material.dart';
import 'package:flutter_course/constant.dart';
import 'package:flutter_course/modules/splash/splash_controller.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';

class Splash extends StatelessWidget {
  SplashController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: gradiantBackground,
          child: Image.asset('assets/images/logo.png'),
        ),
      ),
    );
  }
}
