import 'package:flutter/material.dart';
import 'package:flutter_course/components/custom_button.dart';
import 'package:flutter_course/components/custom_textField.dart';
import 'package:flutter_course/modules/register/register_controller.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import '../../constant.dart';

class Register extends StatelessWidget {
  RegisterController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: gradiantBackground,
          child: ListView(
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.25,
                    child: Divider(
                      color: Colors.white,
                      thickness: 2,
                    ),
                  ),
                  Text(
                    'Register',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.25,
                    child: Divider(
                      color: Colors.white,
                      thickness: 2,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                children: [
                  CustomTextField(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50,
                    hintText: 'Full Name',
                    onChange: (value) {
                      controller.fullName = value;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomTextField(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50,
                    hintText: 'Email',
                    onChange: (value) {
                      controller.email = value;
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomTextField(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50,
                    hintText: 'Password',
                    onChange: (value) {
                      controller.password = value;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomTextField(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50,
                    hintText: 'Confirm Password',
                    onChange: (value) {
                      controller.passwordConfirm = value;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomButton(
                      height: 50,
                      width: MediaQuery.of(context).size.width * 0.7,
                      buttonName: 'Register',
                      ButtonColor: Colors.yellow,
                      onTap: () {
                        onClickRegister();
                      }),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.toNamed('/login');
                    },
                    child: Text(
                      'Already have accaount?Sign in',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onClickRegister() async {
    EasyLoading.show(status: 'loading...');
    await controller.registerOnClick();
    if (controller.registerStatus) {
      EasyLoading.showSuccess(controller.message);
      Get.offNamed('/login');
    } else {
      EasyLoading.showError(
        controller.message,
        duration: Duration(seconds: 10),
        dismissOnTap: true,
      );
      print('Error Here');
    }
  }
}
