import 'dart:convert';

import 'package:flutter_course/config/server_config.dart';
import 'package:flutter_course/models/user.dart';
import 'package:http/http.dart' as http;

class RegisterService {

  var message;

  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.register);

  Future<bool>  register(User user) async {
    var response = await http.post(
      url,
      headers: {
        'Accept': 'Application/json',
      },
      body: {
        'name': user.name,
        'email': user.email,
        'password': user.password,
        'password_confirmation': user.passwordConfirmation,
      },
    );
    print(response.statusCode);
    print(response.body);

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['msg'];
      return true;
    } else if (response.statusCode == 422) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['errors'];
      return false;
    } else {
      return false;
    }
  }
}
