import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_course/components/custom_button.dart';
import 'package:flutter_course/components/custom_checkBox.dart';
import 'package:flutter_course/components/custom_textField.dart';
import 'package:flutter_course/constant.dart';
import 'package:flutter_course/modules/login/login_controller.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class Login extends StatelessWidget {
  LoginController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: gradiantBackground,
          child: ListView(
            children: [
              SizedBox(
                height: 20,
              ),
              Image.asset(
                'assets/images/logo.png',
                fit: BoxFit.contain,
                width: 150,
                height: 150,
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.25,
                    child: Divider(
                      color: Colors.white,
                      thickness: 2,
                    ),
                  ),
                  Text(
                    'sign in',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.25,
                    child: Divider(
                      color: Colors.white,
                      thickness: 2,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                    onTap: () {
                      print('dd');
                    },
                    child: Image.asset(
                      'assets/images/wh.png',
                      fit: BoxFit.contain,
                      width: 30,
                      height: 30,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('dd');
                    },
                    child: Image.asset(
                      'assets/images/fa.png',
                      fit: BoxFit.contain,
                      width: 30,
                      height: 30,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('dd');
                    },
                    child: Image.asset(
                      'assets/images/wh.png',
                      fit: BoxFit.contain,
                      width: 30,
                      height: 30,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('dd');
                    },
                    child: Image.asset(
                      'assets/images/fa.png',
                      fit: BoxFit.contain,
                      width: 30,
                      height: 30,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Text(
                  'Or',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
              Column(
                children: [
                  CustomTextField(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50,
                    hintText: 'Your email address',
                    onChange: (value) {
                      controller.email = value;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  CustomTextField(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 50,
                    hintText: 'password',
                    onChange: (value) {
                      controller.password = value;
                    },
                  ),

                  GestureDetector(
                    onTap: () {
                      print('jj');
                    },
                    child: Text(
                      'Forget your password?',
                      style: TextStyle(
                        color: Colors.white70,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomButton(
                      height: 50,
                      width: MediaQuery.of(context).size.width * 0.7,
                      buttonName: 'sign in',
                      ButtonColor: Colors.grey.withAlpha(188),
                      onTap: () {
                        onClickLogin();
                      }),
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.toNamed('/register');
                    },
                    child: Text(
                      'Dont have accaount?Create now',
                      style: TextStyle(
                        color: Colors.white70,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Obx(() {
                            return CustomCheckBox(
                              size: 25,
                              iconSize: 20,
                              isSelected: controller.checkBoxStatus.value,
                              onTap: () {
                                controller.changeCheckBox();
                              },
                            );
                          }),
                          SizedBox(
                            width: 20,
                          ),
                          Text(
                            "keep me sign in",
                            style: TextStyle(
                                fontSize: 20,
                                fontFamily: "Acaslon Regular",
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onClickLogin() async {
    EasyLoading.show(status: 'loading...');
    await controller.loginOnClick();
    if (controller.loginStatus) {
      EasyLoading.showSuccess(controller.message);
      Get.toNamed('/home');
    } else {
      EasyLoading.showError(
        controller.message,
        duration: Duration(seconds: 2),
        dismissOnTap: true,
      );
      print('Error Here');
    }
  }
}
