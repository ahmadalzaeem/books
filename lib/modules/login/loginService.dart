import 'dart:convert';

import 'package:flutter_course/config/server_config.dart';
import 'package:flutter_course/config/user_information.dart';
import 'package:flutter_course/models/user.dart';
import 'package:flutter_course/native_service/secure_storage.dart';
import 'package:http/http.dart' as http;

class LoginService {
  var message;
  var token ;

  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.login);

  Future<bool> login(User user , bool checkBox) async {
    var response = await http.post(url, headers: {
      'Accept': 'application/json',
    }, body: {
      'email': user.email,
      'password': user.password,
    });
    print(response.statusCode);
    print(response.body);

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['msg'];
      token = jsonResponse['token'];
      UserInformation.USER_TOKEN = token;
      if(checkBox){
        SecureStorage storage = SecureStorage();
        await storage.save('token',UserInformation.USER_TOKEN  );
      }
      return true;
    } else if (response.statusCode == 401) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['error'];
      return false;
    }else if (response.statusCode == 422) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['errors'];
      return false;
    }
    else {
      message = 'server error';
      return false;
    }
  }
}
