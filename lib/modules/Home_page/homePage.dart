import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';

import '../../constant.dart';

class HomePage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: Container(
        width: MediaQuery.of(context).size.width * .6,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Drawer(),
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: gradiantBackground,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        _scaffoldKey.currentState!.openDrawer();
                      },
                      child: Container(
                          child: Icon(Icons.menu, color: Colors.white, size: 30)),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 40,),
              Center(
                child: GestureDetector(
                  onTap: (){
                    print('book');
                  },
                  child: Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width * .8,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Center(
                      child: Text(
                        'View Book',
                        style: TextStyle(
                          fontSize: 30,
                          fontFamily: 'ACaslon Bold',

                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 40,),
              Center(
                child: GestureDetector(
                  onTap: (){
                    print('cat');
                    Get.toNamed('/categories');
                  },
                  child: Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width * .8,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Center(
                      child: Text(
                        'View Categories',
                        style: TextStyle(
                          fontSize: 30,


                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
