import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_course/components/custom_button.dart';
import 'package:flutter_course/constant.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';

class Landing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: gradiantBackground,
          child: Column(
            children: [
              SizedBox(
                height: 10.0,
              ),
//              Image.asset('assets/images/logo.png',
//              fit: BoxFit.contain,
//               width: 150,
//                 height: 150,
//                 ),
              AnimatedTextKit(
                animatedTexts: [
                  TypewriterAnimatedText(
                    'Hello world!',
                    textStyle: const TextStyle(
                      fontSize: 32.0,
                      fontWeight: FontWeight.bold,
                    ),
                    speed: const Duration(milliseconds: 200),
                  ),
                ],

                totalRepeatCount:2,
                pause: const Duration(milliseconds: 1000),
                displayFullTextOnTap: true,
                stopPauseOnTap: true,
              ),
              SizedBox(
                height: 20.0,
              ),
              CustomButton(
                width: MediaQuery.of(context).size.width * .9,
                height: 30,
                buttonName: 'login',
                fontSize: 20,
                onTap: () {
                  Get.offNamed('/login');
                },
              ),
              SizedBox(
                height: 30.0,
              ),
              CustomButton(
                width: MediaQuery.of(context).size.width * .9,
                height: 30,
                buttonName: 'Sign Up',
                fontSize: 20,
                ButtonColor: Colors.redAccent,
                onTap: () {
                  print('kbjhb');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
