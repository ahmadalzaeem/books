import 'package:flutter/cupertino.dart';

const gradiantBackground = BoxDecoration(
  gradient: LinearGradient(
      colors: [
        Color(0xFF3366FF),
        Color(0xFF00CCFF),
      ],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      stops: [0.0, 1.0],
      tileMode: TileMode.clamp),
);
